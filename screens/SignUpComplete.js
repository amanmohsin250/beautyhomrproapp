import React from "react";
import { View, Text, StyleSheet, TouchableOpacity } from "react-native";
import AppbarStyles from "../assets/styles/AppbarStyles";
import { Entypo } from "@expo/vector-icons";

export default function SignUpComplete({ navigation }) {
  return (
    <View style={AppbarStyles.SignContainer}>
      <View style={AppbarStyles.TitleContainer}>
        <View style={AppbarStyles.TouchContainer}></View>
        <Text style={AppbarStyles.TitleText}>Registration Complete</Text>
        <View style={AppbarStyles.rightContainer} />
      </View>
      <View style={styles.message}>
        <Text
          style={{ textAlign: "center", color: "#e2c798", fontWeight: "bold" }}
        >
          Thank you for registering with us. We are currently reviewing your
          application, you should hear from us shortly.
        </Text>
      </View>
      <TouchableOpacity
        style={{ alignItems: "center" }}
        onPress={() => navigation.navigate("Login")}
      >
        <Entypo name="login" size={24} color="#e2c798" />
      </TouchableOpacity>
    </View>
  );
}

const styles = StyleSheet.create({
  message: {
    marginBottom: 50,
    marginTop: "60%",
  },
});

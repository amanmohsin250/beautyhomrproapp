import React, { useState } from "react";
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  Image,
  TextInput,
  TouchableWithoutFeedback,
  Keyboard,
} from "react-native";
import AppbarStyles from "../assets/styles/AppbarStyles";
import FormStyles from "../assets/styles/FormContainer";
import { MaterialCommunityIcons as Icons } from "@expo/vector-icons";

export default function SignUp({ navigation }) {
  const [hidePass, setHidePass] = useState(true);

  const HandlePasswordVisibility = () => {
    setHidePass(!hidePass);
  };
  return (
    <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
      <View style={AppbarStyles.SignContainer}>
        <View style={AppbarStyles.TitleContainer}>
          <TouchableOpacity
            style={AppbarStyles.TouchContainer}
            onPress={() => navigation.goBack()}
          >
            <Image source={require("../assets/images/Back.png")} />
            <Text style={AppbarStyles.BackText}>Back</Text>
          </TouchableOpacity>
          <Text style={AppbarStyles.TitleText}>Register</Text>
          <View style={AppbarStyles.rightContainer} />
        </View>
        <View style={styles.inputsContainer}>
          <View style={FormStyles.input}>
            <TextInput placeholder="Full Name" />
          </View>
          <View style={FormStyles.input}>
            <TextInput placeholder="Sex" />
          </View>
          <View style={FormStyles.input}>
            <TextInput placeholder="Email" />
          </View>
          <View style={FormStyles.input}>
            <TextInput placeholder="Phone" keyboardType="numeric" />
          </View>
          <View style={FormStyles.password}>
            <TextInput
              placeholder="Password"
              secureTextEntry={hidePass}
              style={{ flex: 1 }}
            />
            <TouchableOpacity onPress={HandlePasswordVisibility}>
              <Icons name={hidePass ? "eye" : "eye-off"} size={20} />
            </TouchableOpacity>
          </View>
          <View style={FormStyles.input}>
            <TextInput placeholder="City" />
          </View>
        </View>
        <View style={FormStyles.bottom}>
          <TouchableOpacity
            onPress={() => navigation.navigate("SignUpComplete")}
          >
            <View style={FormStyles.SignInButton}>
              <Text style={{ fontSize: 20 }}>Submit</Text>
            </View>
          </TouchableOpacity>
        </View>
      </View>
    </TouchableWithoutFeedback>
  );
}

const styles = StyleSheet.create({
  inputsContainer: {
    marginTop: "15%",
  },
});

import React, { useState } from "react";
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  Image,
  TextInput,
  TouchableWithoutFeedback,
  Keyboard,
} from "react-native";
import AppbarStyles from "../assets/styles/AppbarStyles";
import FormStyles from "../assets/styles/FormContainer";
import { Ionicons } from "@expo/vector-icons";

export default function SignUp({ navigation }) {
  const [hidePass, setHidePass] = useState(true);

  const HandlePasswordVisibility = () => {
    setHidePass(!hidePass);
  };
  return (
    <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
      <View style={AppbarStyles.SignContainer}>
        <View style={AppbarStyles.TitleContainer}>
          <TouchableOpacity
            style={AppbarStyles.TouchContainer}
            onPress={() => navigation.goBack()}
          >
            <Ionicons name="close" size={24} color="black" />
          </TouchableOpacity>
          <Text style={AppbarStyles.TitleText}>Enter Details</Text>
          <View style={AppbarStyles.rightContainer} />
        </View>
        <View style={styles.inputsContainer}>
          <Text style={FormStyles.label}>Account Number</Text>
          <View style={FormStyles.input}>
            <TextInput placeholder="e.g 00112233" keyboardType="numeric" />
          </View>
          <Text style={FormStyles.label}>Account Name</Text>
          <View style={FormStyles.input}>
            <TextInput placeholder="e.g Charles Smith" />
          </View>
          <Text style={FormStyles.label}>Account Sort Code</Text>
          <View style={FormStyles.input}>
            <TextInput placeholder="e.g 00-11-22" />
          </View>
          <Text style={FormStyles.label}>Bank Name</Text>
          <View style={FormStyles.input}>
            <TextInput placeholder="e.g Barclays" />
          </View>
        </View>
        <View style={FormStyles.bottom}>
          <TouchableOpacity>
            <View style={FormStyles.SignInButton}>
              <Text style={{ fontSize: 20 }}>SAVE</Text>
            </View>
          </TouchableOpacity>
        </View>
      </View>
    </TouchableWithoutFeedback>
  );
}

const styles = StyleSheet.create({
  inputsContainer: {
    marginTop: "15%",
  },
});

import React from "react";
import { View, Text, StyleSheet, TouchableOpacity } from "react-native";
import AppbarStyles from "../assets/styles/AppbarStyles";
import { Ionicons, FontAwesome5 } from "@expo/vector-icons";
import { Divider } from "react-native-paper";

export default function Home({ navigation }) {
  return (
    <View style={styles.container}>
      <View style={AppbarStyles.TitleContainer}>
        <View style={AppbarStyles.TouchContainer}>
          <TouchableOpacity onPress={() => navigation.navigate("Profile")}>
            <Ionicons
              name="person-outline"
              size={24}
              color="black"
              style={{ marginLeft: 20 }}
            />
          </TouchableOpacity>
        </View>
        <Text style={AppbarStyles.TitleText}>Home</Text>
        <View style={AppbarStyles.rightContainer}>
          <TouchableOpacity onPress={() => navigation.replace("Login")}>
            <Ionicons
              name="log-out-outline"
              size={24}
              color="black"
              style={{ marginLeft: 20 }}
            />
          </TouchableOpacity>
        </View>
      </View>
      <View style={styles.HomeDateTitleContainer}>
        <Text style={styles.HomeDateTitleText}>SUNDAY</Text>
        <Text style={styles.HomeDateTitleText}>3rd</Text>
        <Text style={styles.HomeDateTitleText}>February</Text>
      </View>
      <Divider />
      <Text style={styles.HeadingText}>THIS WEEK</Text>
      <View style={styles.HomeEarnedContainer}>
        <Text style={styles.HomeDateTitleText}>£0.00</Text>
        <Text style={styles.EarnedText}>Earned</Text>
      </View>
      <Divider />
      <Text style={styles.HeadingText}>YOUR STATS</Text>
      <View style={styles.HomeStatsContainer}>
        <View style={{ alignItems: "center" }}>
          <Text style={styles.HomeDateTitleText}>0%</Text>
          <Text>Acceptance Rate</Text>
        </View>
        <View style={{ alignItems: "center" }}>
          <Text style={styles.HomeDateTitleText}>0</Text>
          <Text>Peak Hours</Text>
        </View>
        <View style={{ alignItems: "center" }}>
          <Text style={styles.HomeDateTitleText}>0</Text>
          <Text>Quality Score</Text>
        </View>
      </View>
      <TouchableOpacity
        onPress={() => navigation.navigate("CurrentAppointment")}
      >
        <View style={styles.FloatingButton}>
          <FontAwesome5 name="running" size={24} color="white" />
        </View>
      </TouchableOpacity>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    marginTop: "7%",
    flex: 1,
    backgroundColor: "white",
  },
  HomeDateTitleContainer: {
    height: "40%",
    justifyContent: "center",
    alignItems: "center",
  },
  HomeDateTitleText: {
    color: "#e2c798",
    fontSize: 24,
    fontWeight: "bold",
  },
  HeadingText: {
    marginTop: 30,
    marginLeft: 20,
    fontSize: 18,
    color: "rgba(189, 195, 199,1.0)",
    fontWeight: "bold",
  },
  HomeEarnedContainer: {
    height: "20%",
    justifyContent: "center",
    alignItems: "center",
  },
  EarnedText: {
    color: "rgba(189, 195, 199,1.0)",
    marginTop: 10,
  },
  HomeStatsContainer: {
    flexDirection: "row",
    justifyContent: "space-evenly",
    height: "15%",
    alignItems: "center",
  },
  FloatingButton: {
    backgroundColor: "#e2c798",
    position: "absolute",
    width: 50,
    height: 50,
    alignItems: "center",
    justifyContent: "center",
    right: 30,
    bottom: 0,
    borderRadius: 50,
  },
});

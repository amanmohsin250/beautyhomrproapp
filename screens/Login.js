import React, { useState } from "react";
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  TextInput,
  TouchableWithoutFeedback,
  Keyboard,
} from "react-native";
import { MaterialCommunityIcons as Icons } from "@expo/vector-icons";
import AppbarStyles from "../assets/styles/AppbarStyles";
import FormStyles from "../assets/styles/FormContainer";

export default function Login({ navigation }) {
  const [EmailText, setEmailText] = useState("");
  const [hidePass, setHidePass] = useState(true);
  const [PasswordText, setPasswordText] = useState("");
  const [signInErr, setSignInErr] = useState(false);
  const EmailHandler = (enteredText) => {
    setEmailText(enteredText);
  };
  const PasswordHandler = (enteredText) => {
    setPasswordText(enteredText);
  };
  const HandlePasswordVisibility = () => {
    setHidePass(!hidePass);
  };

  const SignInHandler = () => {
    if (EmailText == "Aman" && PasswordText == "Hello") {
      setSignInErr(false);
      navigation.replace("Home");
    } else {
      setSignInErr(true);
    }
  };
  const SignUpClickHandler = () => {
    navigation.navigate("SignUp");
  };
  return (
    <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
      <View style={AppbarStyles.SignContainer}>
        <View style={AppbarStyles.TitleContainer}>
          <View style={AppbarStyles.TouchContainer}></View>
          <Text style={AppbarStyles.TitleText}>Log In</Text>
          <View style={AppbarStyles.rightContainer} />
        </View>
        <View style={styles.inputsContainer}>
          <View style={FormStyles.input}>
            <TextInput
              placeholder="Email"
              onChangeText={EmailHandler}
              value={EmailText}
            />
          </View>
          <View style={FormStyles.password}>
            <TextInput
              placeholder="password"
              secureTextEntry={hidePass}
              style={{ flex: 1 }}
              onChangeText={PasswordHandler}
              value={PasswordText}
            />
            <TouchableOpacity onPress={HandlePasswordVisibility}>
              <Icons name={hidePass ? "eye" : "eye-off"} size={20} />
            </TouchableOpacity>
          </View>
        </View>
        {signInErr ? (
          <Text style={FormStyles.error}>Invalid Email or Password</Text>
        ) : null}
        <TouchableOpacity onPress={SignInHandler}>
          <View style={FormStyles.SignInButton}>
            <Text style={{ fontSize: 20 }}>Sign In</Text>
          </View>
        </TouchableOpacity>
        <TouchableOpacity>
          <Text style={FormStyles.forgotText}>Forgot Password?</Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={SignUpClickHandler}>
          <Text style={FormStyles.forgotText}>Sign Up</Text>
        </TouchableOpacity>
      </View>
    </TouchableWithoutFeedback>
  );
}

const styles = StyleSheet.create({
  inputsContainer: {
    marginTop: "25%",
  },
});

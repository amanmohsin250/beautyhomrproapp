import React from "react";
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  ScrollView,
} from "react-native";
import AppbarStyles from "../assets/styles/AppbarStyles";
import {
  Ionicons,
  Entypo,
  Fontisto,
  MaterialIcons,
  AntDesign,
} from "@expo/vector-icons";
import Card from "../components/Card";
import { Avatar, Divider } from "react-native-paper";

export default function Profile({ navigation }) {
  return (
    <View style={AppbarStyles.SignContainer}>
      <View style={AppbarStyles.TitleContainer}>
        <TouchableOpacity
          style={AppbarStyles.TouchContainer}
          onPress={() => navigation.goBack()}
        >
          <Ionicons name="close" size={24} color="black" />
        </TouchableOpacity>
        <Text style={AppbarStyles.TitleText}>Profile</Text>
        <View style={AppbarStyles.rightContainer} />
      </View>
      <View style={styles.ProfilecardContainer}>
        <Card>
          <View style={styles.ProfileImageConatiner}>
            <View style={styles.IconTextContainer}>
              <Avatar.Image
                source={require("../assets/images/Avatar.png")}
                size={70}
              />
            </View>
            <View style={styles.IconTextContainer}>
              <Text style={styles.ProfileNameText}>Aman</Text>
            </View>
          </View>
          <Divider />
          <View style={styles.ProfileImageConatiner}>
            <View style={styles.IconTextContainer}>
              <Entypo name="bar-graph" size={18} color="#e2c798" />
              <Text style={styles.IconText}>0 Ratings</Text>
            </View>
            <View style={styles.IconTextContainer}>
              <Fontisto name="person" size={18} color="#e2c798" />
              <Text style={styles.IconText}>Expert</Text>
            </View>
          </View>
        </Card>
      </View>
      <ScrollView>
        <TouchableOpacity>
          <Card>
            <View style={styles.CardContainer}>
              <View style={styles.AbbreviationsContainer}>
                <Text style={styles.AbbreviationsText}>CL</Text>
              </View>
              <View>
                <Text>Call ONFLEEK Team</Text>
              </View>
              <View>
                <Ionicons name="call-sharp" size={24} color="#e2c798" />
              </View>
            </View>
          </Card>
        </TouchableOpacity>

        <TouchableOpacity>
          <Card>
            <View style={styles.CardContainer}>
              <View style={styles.AbbreviationsContainer}>
                <Text style={styles.AbbreviationsText}>EM</Text>
              </View>
              <View>
                <Text>Email ONFLEEK Team</Text>
              </View>
              <View>
                <MaterialIcons name="email" size={24} color="#e2c798" />
              </View>
            </View>
          </Card>
        </TouchableOpacity>

        <TouchableOpacity>
          <Card>
            <View style={styles.CardContainer}>
              <View style={styles.AbbreviationsContainer}>
                <Text style={styles.AbbreviationsText}>FQ</Text>
              </View>
              <View>
                <Text>FAQ</Text>
              </View>
              <View>
                <AntDesign name="questioncircle" size={24} color="#e2c798" />
              </View>
            </View>
          </Card>
        </TouchableOpacity>
      </ScrollView>
    </View>
  );
}

const styles = StyleSheet.create({
  ProfileImageConatiner: {
    flexDirection: "row",
    alignItems: "center",
    marginVertical: 20,
  },
  ProfileNameText: {
    fontSize: 18,
    fontWeight: "bold",
    color: "#e2c798",
  },
  IconTextContainer: {
    flexDirection: "row",
    alignItems: "center",
    width: "50%",
    justifyContent: "center",
  },
  IconText: {
    marginLeft: 10,
    fontSize: 14,
    fontWeight: "bold",
    color: "#e2c798",
  },
  AbbreviationsContainer: {
    backgroundColor: "rgba(189, 195, 199,0.5)",
    width: "15%",
    justifyContent: "center",
    alignItems: "center",
    padding: 10,
  },
  AbbreviationsText: {
    color: "white",
    fontSize: 16,
    fontWeight: "bold",
  },
  CardContainer: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
  },
});

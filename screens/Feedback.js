import React, { useState } from "react";
import {
  View,
  Text,
  StyleSheet,
  ScrollView,
  TouchableOpacity,
} from "react-native";
import AppbarStyles from "../assets/styles/AppbarStyles";
import StarRating from "react-native-star-rating";
import { Divider, Checkbox } from "react-native-paper";
import FormStyles from "../assets/styles/FormContainer";

export default function Feedback() {
  const [starCount, setStarCount] = useState(0);
  const [staircase, setStaircase] = useState(false);
  const [smoking, setSmoking] = useState(false);
  const [children, setChildren] = useState(false);
  const [cats, setCats] = useState(false);
  const [dogs, setDogs] = useState(false);
  const [limiitedspace, setLimitedSpace] = useState(false);
  const [parking, setParking] = useState(false);
  const [none, setNone] = useState(false);
  const onStarRatingPress = (rating) => {
    setStarCount(rating);
  };
  return (
    <View style={styles.container}>
      <View style={AppbarStyles.TitleContainer}>
        <View style={AppbarStyles.TouchContainer}></View>
        <Text style={AppbarStyles.TitleText}>Feedback</Text>
        <View style={AppbarStyles.rightContainer} />
      </View>
      <View style={styles.ratingContainer}>
        <Text style={styles.RatingHeading}>How was Test Appointment</Text>
        <StarRating
          disabled={false}
          maxStars={5}
          rating={starCount}
          selectedStar={(rating) => onStarRatingPress(rating)}
          fullStarColor="#e2c798"
        />
      </View>
      <Divider />
      <Text style={styles.RatingHeading}>Appointment Notes</Text>
      <View style={{ marginBottom: 20 }}>
        <Text style={styles.NotesQuestion}>
          Are the following relevant to you client's residence? (Tap all that
          apply)
        </Text>
        <ScrollView
          style={{ height: "40%" }}
          showsVerticalScrollIndicator={false}
        >
          <View style={FormStyles.checkboxContainer}>
            <View style={FormStyles.checkbox}>
              <Checkbox
                style={FormStyles.checkbox}
                status={staircase ? "checked" : "unchecked"}
                onPress={() => {
                  setStaircase(!staircase);
                }}
              />
            </View>
            <View style={FormStyles.checkTextContainer}>
              <Text style={{ fontSize: 16 }}>Access via staircase</Text>
            </View>
          </View>

          <View style={FormStyles.checkboxContainer}>
            <View style={FormStyles.checkbox}>
              <Checkbox
                style={FormStyles.checkbox}
                status={smoking ? "checked" : "unchecked"}
                onPress={() => {
                  setSmoking(!smoking);
                }}
              />
            </View>
            <View style={FormStyles.checkTextContainer}>
              <Text style={{ fontSize: 16 }}>Smoking household</Text>
            </View>
          </View>

          <View style={FormStyles.checkboxContainer}>
            <View style={FormStyles.checkbox}>
              <Checkbox
                style={FormStyles.checkbox}
                status={children ? "checked" : "unchecked"}
                onPress={() => {
                  setChildren(!children);
                }}
              />
            </View>
            <View style={FormStyles.checkTextContainer}>
              <Text style={{ fontSize: 16 }}>Children Present</Text>
            </View>
          </View>

          <View style={FormStyles.checkboxContainer}>
            <View style={FormStyles.checkbox}>
              <Checkbox
                style={FormStyles.checkbox}
                status={cats ? "checked" : "unchecked"}
                onPress={() => {
                  setCats(!cats);
                }}
              />
            </View>
            <View style={FormStyles.checkTextContainer}>
              <Text style={{ fontSize: 16 }}>Cats Present</Text>
            </View>
          </View>

          <View style={FormStyles.checkboxContainer}>
            <View style={FormStyles.checkbox}>
              <Checkbox
                style={FormStyles.checkbox}
                status={dogs ? "checked" : "unchecked"}
                onPress={() => {
                  setDogs(!dogs);
                }}
              />
            </View>
            <View style={FormStyles.checkTextContainer}>
              <Text style={{ fontSize: 16 }}>Dogs Present</Text>
            </View>
          </View>

          <View style={FormStyles.checkboxContainer}>
            <View style={FormStyles.checkbox}>
              <Checkbox
                style={FormStyles.checkbox}
                status={limiitedspace ? "checked" : "unchecked"}
                onPress={() => {
                  setLimitedSpace(!limiitedspace);
                }}
              />
            </View>
            <View style={FormStyles.checkTextContainer}>
              <Text style={{ fontSize: 16 }}>Limited Space</Text>
            </View>
          </View>

          <View style={FormStyles.checkboxContainer}>
            <View style={FormStyles.checkbox}>
              <Checkbox
                style={FormStyles.checkbox}
                status={parking ? "checked" : "unchecked"}
                onPress={() => {
                  setParking(!parking);
                }}
              />
            </View>
            <View style={FormStyles.checkTextContainer}>
              <Text style={{ fontSize: 16 }}>Resident's parking available</Text>
            </View>
          </View>

          <View style={FormStyles.checkboxContainer}>
            <View style={FormStyles.checkbox}>
              <Checkbox
                style={FormStyles.checkbox}
                status={none ? "checked" : "unchecked"}
                onPress={() => {
                  setNone(!none);
                }}
              />
            </View>
            <View style={FormStyles.checkTextContainer}>
              <Text style={{ fontSize: 16 }}>None of the above</Text>
            </View>
          </View>
        </ScrollView>
      </View>
      <View style={FormStyles.bottom}>
        <TouchableOpacity>
          <View style={FormStyles.SignInButton}>
            <Text style={{ fontSize: 20 }}>Submit</Text>
          </View>
        </TouchableOpacity>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: "7%",
    backgroundColor: "white",
  },
  ratingContainer: {
    marginVertical: 20,
    marginHorizontal: 80,
  },
  RatingHeading: {
    marginVertical: 10,
    fontSize: 16,
    fontWeight: "bold",
    textAlign: "center",
  },
  NotesQuestion: {
    marginVertical: 10,
    fontSize: 14,
    fontWeight: "bold",
    marginHorizontal: 20,
  },
});

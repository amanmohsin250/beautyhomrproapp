import React from "react";
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  ScrollView,
} from "react-native";
import AppbarStyles from "../assets/styles/AppbarStyles";
import { Feather } from "@expo/vector-icons";
import { Divider } from "react-native-paper";

const earningList = [
  {
    Abbreviations: "TA",
    Name: "Test Appointment",
    Date: "January 31,2021",
    ID: "185155",
    earned: "0",
    pending: "0",
  },
  {
    Abbreviations: "TA",
    Name: "Test Appointment",
    Date: "January 22,2021",
    ID: "183759",
    earned: "0",
    pending: "0",
  },
  {
    Abbreviations: "TA",
    Name: "Test Appointment",
    Date: "January 21,2021",
    ID: "183602",
    earned: "0",
    pending: "0",
  },
  {
    Abbreviations: "TA",
    Name: "Test Appointment",
    Date: "January 15,2021",
    ID: "182734",
    earned: "0",
    pending: "0",
  },
  {
    Abbreviations: "GF",
    Name: "Georgia Farris",
    Date: "February 10,2021",
    ID: "185158",
    earned: "0",
    pending: "0",
  },
];

const EarningsList = () => {
  return (
    <View>
      {earningList.map((item, index) => (
        <View key={index}>
          <View style={styles.ListItemContainer}>
            <View style={styles.AbbreviationsContainer}>
              <Text style={styles.AbbreviationsText}>{item.Abbreviations}</Text>
            </View>
            <View>
              <Text style={{ fontWeight: "bold" }}>{item.Name}</Text>
              <Text
                style={{ fontWeight: "bold", color: "rgba(189, 195, 199,1.0)" }}
              >
                {item.Date}
              </Text>
              <Text style={{ fontWeight: "bold" }}>
                Tip for Booking ID {item.ID}
              </Text>
            </View>
            <View>
              <Text>£{item.earned}</Text>
              <Text
                style={{ fontWeight: "bold", color: "#e2c798", fontSize: 12 }}
              >
                Pending
              </Text>
              <Text>£{item.pending}</Text>
            </View>
          </View>
          <Divider />
        </View>
      ))}
    </View>
  );
};

export default function Earnings({ navigation }) {
  return (
    <View style={styles.container}>
      <View style={AppbarStyles.TitleContainer}>
        <View style={AppbarStyles.TouchContainer}></View>
        <Text style={AppbarStyles.TitleText}>Earnings</Text>
        <View style={AppbarStyles.rightContainer} />
      </View>
      <View style={styles.AccountDetailsContainer}>
        <View style={{ marginLeft: 20 }}>
          <Text></Text>
          <Text style={{ fontSize: 18, fontWeight: "bold", color: "white" }}>
            £0.00
          </Text>
          <Text style={{ color: "white" }}>ACCOUNT BALANCE</Text>
        </View>
        <View style={{ alignItems: "flex-end", marginRight: 20 }}>
          <TouchableOpacity
            onPress={() => navigation.navigate("AccountDetails")}
          >
            <Feather name="edit" size={24} color="white" />
          </TouchableOpacity>
          <Text style={{ color: "white" }}>A/C No.</Text>
          <Text style={{ color: "white" }}>ACCOUNT DETAILS</Text>
        </View>
      </View>
      <View style={styles.EarningListContainer}>
        <ScrollView>
          <EarningsList />
        </ScrollView>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    marginTop: "7%",
    flex: 1,
    backgroundColor: "white",
  },
  AccountDetailsContainer: {
    backgroundColor: "#e2c798",
    flexDirection: "row",
    justifyContent: "space-between",
    height: "15%",
    alignItems: "center",
  },
  EarningListContainer: {
    marginVertical: 30,
  },
  ListItemContainer: {
    flexDirection: "row",
    justifyContent: "space-between",
    padding: 20,
  },
  AbbreviationsContainer: {
    backgroundColor: "rgba(189, 195, 199,0.5)",
    width: "15%",
    justifyContent: "center",
    alignItems: "center",
  },
  AbbreviationsText: {
    color: "white",
    fontSize: 20,
    fontWeight: "bold",
  },
});

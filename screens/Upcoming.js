import React, { useState } from "react";
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  ScrollView,
} from "react-native";
import Card from "../components/Card";
import AppbarStyles from "../assets/styles/AppbarStyles";

const upcomingList = [
  {
    dayDate: "SUNDAY 3",
    month: "FEBRUARY",
    timing: "7:00 - 8:00 PM",
    name: "TEST APPT - LVL",
    subtitle: "TEST APPT - LVL",
    repeated: 15,
  },
  {
    dayDate: "THURSDAY 7",
    month: "FEBRUARY",
    timing: "7:00 - 8:00 PM",
    name: "TEST APPT - Kerastase Blow Dry and Day Make-up",
    subtitle: "TEST APPT - Kerastase Blow Dry and Day Make-up",
    repeated: 27,
  },
  {
    dayDate: "SATURDAY 9",
    month: "FEBRUARY",
    timing: "7:00 - 8:00 PM",
    name: "TEST APPT - Massage",
    subtitle: "TEST APPT - Massage",
    repeated: 60,
  },
];

export default function Upcoming({ navigation }) {
  const [activeTabs, setActiveTabs] = useState("Upcoming");
  return (
    <View style={AppbarStyles.SignContainer}>
      <View style={AppbarStyles.TitleContainer}>
        <View style={AppbarStyles.TouchContainer}></View>
        <Text style={AppbarStyles.TitleText}>Appointments</Text>
        <View style={AppbarStyles.rightContainer} />
      </View>
      <View style={styles.ProfileTabsContainer}>
        {activeTabs == "Upcoming" ? (
          <TouchableOpacity
            activeOpacity={0.8}
            style={styles.ActiveTab}
            onPress={() => setActiveTabs("Upcoming")}
          >
            <View>
              <Text style={styles.ActiveTabText}>UPCOMING</Text>
            </View>
          </TouchableOpacity>
        ) : (
          <TouchableOpacity
            activeOpacity={0.8}
            style={styles.InactiveTab}
            onPress={() => setActiveTabs("Upcoming")}
          >
            <View>
              <Text style={styles.TabText}>UPCOMING</Text>
            </View>
          </TouchableOpacity>
        )}

        {activeTabs == "Cancelled" ? (
          <TouchableOpacity
            activeOpacity={0.8}
            style={styles.ActiveTab}
            onPress={() => setActiveTabs("Cancelled")}
          >
            <View>
              <Text style={styles.ActiveTabText}>CANCELLED</Text>
            </View>
          </TouchableOpacity>
        ) : (
          <TouchableOpacity
            activeOpacity={0.8}
            style={styles.InactiveTab}
            onPress={() => setActiveTabs("Cancelled")}
          >
            <View>
              <Text style={styles.TabText}>CANCELLED</Text>
            </View>
          </TouchableOpacity>
        )}
      </View>
      <ScrollView>
        {activeTabs == "Upcoming"
          ? upcomingList.map((item, index) => (
              <TouchableOpacity
                key={index}
                onPress={() =>
                  navigation.navigate("AppointmentDetails", {
                    dayDate: item.dayDate,
                    month: item.month,
                    timing: item.timing,
                    name: item.name,
                    subtitle: item.subtitle,
                    repeated: item.repeated,
                  })
                }
              >
                <Card>
                  <View style={styles.ListItemContainer}>
                    <View style={styles.dayDateContainer}>
                      <Text style={styles.CardHeadingText}>{item.dayDate}</Text>
                      <Text style={styles.CardHeadingText}>{item.month}</Text>
                      <Text style={styles.timingText}>{item.timing}</Text>
                    </View>
                    <View style={styles.nameContainer}>
                      <Text style={styles.CardHeadingText}>{item.name}</Text>
                      <Text style={styles.timingText}>{item.subtitle}</Text>
                      <Text style={styles.coloredText}>Existing customer:</Text>
                      <Text style={styles.coloredText}>
                        Repeated {item.repeated} times
                      </Text>
                    </View>
                  </View>
                </Card>
              </TouchableOpacity>
            ))
          : null}
      </ScrollView>
    </View>
  );
}

const styles = StyleSheet.create({
  ProfileTabsContainer: {
    marginTop: 15,
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
  },
  ActiveTab: {
    width: "50%",
    paddingBottom: 10,
    borderBottomWidth: 2,
    borderBottomColor: "#e2c798",
  },
  ActiveTabText: {
    textAlign: "center",
    color: "#e2c798",
  },
  TabText: {
    textAlign: "center",
  },
  InactiveTab: {
    width: "50%",
    paddingBottom: 10,
  },
  ListItemContainer: {
    flexDirection: "row",
  },
  dayDateContainer: {
    width: "40%",
  },
  nameContainer: {
    width: "60%",
  },
  CardHeadingText: {
    fontSize: 16,
    fontWeight: "bold",
  },
  timingText: {
    fontSize: 12,
    color: "rgba(189, 195, 199,1.0)",
  },
  coloredText: {
    fontSize: 14,
    fontWeight: "bold",
    color: "#e2c798",
  },
});

import React, { useState } from "react";
import { View, Text, StyleSheet, TouchableOpacity } from "react-native";
import { Ionicons, Entypo, AntDesign } from "@expo/vector-icons";
import AppbarStyles from "../assets/styles/AppbarStyles";
import Card from "../components/Card";
import { Divider, Switch } from "react-native-paper";
import FormStyles from "../assets/styles/FormContainer";

export default function CurrentAppointment({ navigation }) {
  const [enRoute, setEnRoute] = useState(false);
  const [Arrived, setArrived] = useState(false);
  const [EndService, setEndService] = useState(false);
  const EndServiceHandler = () => {
    setEndService(!EndService);
    navigation.navigate("Feedback");
  };
  return (
    <View style={styles.container}>
      <View style={AppbarStyles.TitleContainer}>
        <TouchableOpacity
          style={AppbarStyles.TouchContainer}
          onPress={() => navigation.goBack()}
        >
          <Ionicons name="close" size={24} color="black" />
        </TouchableOpacity>
        <Text style={AppbarStyles.TitleText}>CURRENT APPOINTMENT</Text>
        <View style={AppbarStyles.rightContainer} />
      </View>
      <Card>
        <View style={styles.cardContainer}>
          <View style={styles.AbbreviationsContainer}>
            <Text style={styles.AbbreviationsText}>TA</Text>
          </View>
          <View style={styles.ApptNameAddCont}>
            <Text style={{ fontSize: 16 }}>Test Appointment</Text>
            <Text style={{ fontSize: 16 }}>
              81 Farringdon Street London, EC4A
            </Text>
          </View>
        </View>
      </Card>
      <View style={styles.ButtonsContainer}>
        <TouchableOpacity style={styles.Button}>
          <View>
            <Text style={styles.ButtonText}>Call Customer</Text>
          </View>
        </TouchableOpacity>
        <TouchableOpacity style={styles.Button}>
          <View>
            <Text style={styles.ButtonText}>Text Customer</Text>
          </View>
        </TouchableOpacity>
        <TouchableOpacity style={styles.Button}>
          <View>
            <Text style={styles.ButtonText}>Contact HQ</Text>
          </View>
        </TouchableOpacity>
      </View>
      <View style={styles.DetailsContainer}>
        <View style={styles.IconTextContainer}>
          <Entypo name="shopping-cart" size={24} color="#e2c798" />
          <Text style={styles.IconText}>Service</Text>
        </View>
        <View>
          <Text>TEST APPT - Massage</Text>
        </View>
      </View>
      <Divider />
      <View style={styles.DetailsContainer}>
        <View style={styles.IconTextContainer}>
          <AntDesign name="clockcircleo" size={24} color="#e2c798" />
          <Text style={styles.IconText}>Time</Text>
        </View>
        <View>
          <Text>Sun Feb 3rd 8:pm</Text>
        </View>
      </View>
      <Divider />
      <View style={styles.DetailsContainer}>
        <View style={styles.IconTextContainer}>
          <Ionicons name="speedometer-outline" size={24} color="#e2c798" />
          <Text style={styles.IconText}>Duration</Text>
        </View>
        <View>
          <Text>60 minutes</Text>
        </View>
      </View>
      <Divider />
      <View style={styles.HistoryHeadingContainer}>
        <Ionicons name="book-outline" size={30} color="black" />
        <Text style={styles.HistoryHeading}>CUSTOMER HISTORY</Text>
      </View>
      <View style={{ alignItems: "center" }}>
        <Text style={styles.coloredText}>
          Existing Customer: Repeated 60 times
        </Text>
      </View>
      <View style={FormStyles.bottom}>
        <View style={styles.SwitchesContainer}>
          <View style={{ alignItems: "center" }}>
            <Switch
              value={enRoute}
              onValueChange={() => setEnRoute(!enRoute)}
              color="#e2c798"
            />
            <Text>En Route</Text>
          </View>
          <View style={{ alignItems: "center" }}>
            <Switch
              value={Arrived}
              onValueChange={() => setArrived(!Arrived)}
              color="#e2c798"
            />
            <Text>Arrived</Text>
          </View>
          <View style={{ alignItems: "center" }}>
            <Switch
              value={EndService}
              onValueChange={EndServiceHandler}
              color="#e2c798"
            />
            <Text>End Service</Text>
          </View>
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: "7%",
    backgroundColor: "white",
  },
  cardContainer: {
    flexDirection: "row",
    //justifyContent: "center",
    alignItems: "center",
  },
  AbbreviationsContainer: {
    backgroundColor: "rgba(189, 195, 199,0.5)",
    width: "15%",
    justifyContent: "center",
    alignItems: "center",
  },
  AbbreviationsText: {
    color: "white",
    fontSize: 20,
    fontWeight: "bold",
  },
  ApptNameAddCont: {
    width: "50%",
    marginLeft: 20,
  },
  ButtonsContainer: {
    flexDirection: "row",
    justifyContent: "space-between",
    marginVertical: 10,
    marginHorizontal: 10,
  },
  Button: {
    backgroundColor: "#e2c798",
    padding: 10,
    borderRadius: 10,
  },
  ButtonText: {
    color: "white",
  },
  DetailsContainer: {
    marginVertical: 20,
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    marginHorizontal: 10,
  },
  IconTextContainer: {
    flexDirection: "row",
    alignItems: "center",
  },
  IconText: {
    marginLeft: 10,
  },
  HistoryHeadingContainer: {
    flexDirection: "row",
    marginVertical: 10,
    justifyContent: "center",
    alignItems: "center",
  },
  HistoryHeading: {
    marginLeft: 10,
    fontSize: 16,
    fontWeight: "bold",
  },
  coloredText: {
    fontSize: 14,
    fontWeight: "bold",
    color: "#e2c798",
  },
  SwitchesContainer: {
    flexDirection: "row",
    justifyContent: "space-between",
    marginHorizontal: 20,
  },
});

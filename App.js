import React from "react";
import Login from "./screens/Login";
import SignUp from "./screens/SignUp";
import SignUpComplete from "./screens/SignUpComplete";
import BottomTabs from "./screens/BottomTabs";
import AccountDetails from "./screens/AccountDetails";
import Profile from "./screens/Profile";
import AppointmentDetails from "./screens/AppointmentDetails";
import CurrentAppointment from "./screens/CurrentAppointment";
import Feedback from "./screens/Feedback";

import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";

const Stack = createStackNavigator();

export default function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="Login" headerMode="none">
        <Stack.Screen name="Login" component={Login} />
        <Stack.Screen name="SignUp" component={SignUp} />
        <Stack.Screen name="SignUpComplete" component={SignUpComplete} />
        <Stack.Screen name="Home" component={BottomTabs} />
        <Stack.Screen name="AccountDetails" component={AccountDetails} />
        <Stack.Screen name="Profile" component={Profile} />
        <Stack.Screen
          name="AppointmentDetails"
          component={AppointmentDetails}
        />
        <Stack.Screen
          name="CurrentAppointment"
          component={CurrentAppointment}
        />
        <Stack.Screen name="Feedback" component={Feedback} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

import React from "react";
import { View, Text, StyleSheet, TouchableOpacity } from "react-native";
import AppbarStyles from "../assets/styles/AppbarStyles";
import FormStyles from "../assets/styles/FormContainer";
import { Ionicons } from "@expo/vector-icons";
import { Divider } from "react-native-paper";

export default function AppointmentDetails({ navigation, route }) {
  const { dayDate, month, timing, name, subtitle, repeated } = route.params;
  return (
    <View style={styles.container}>
      <View style={AppbarStyles.TitleContainer}>
        <TouchableOpacity
          style={AppbarStyles.TouchContainer}
          onPress={() => navigation.goBack()}
        >
          <Ionicons name="close" size={24} color="black" />
        </TouchableOpacity>
        <Text style={AppbarStyles.TitleText}>Appointment Details</Text>
        <View style={AppbarStyles.rightContainer} />
      </View>

      <View style={styles.AppointmentDetailsContainer}>
        <View style={styles.dayDateContainer}>
          <Text style={styles.CardHeadingText}>{dayDate}</Text>
          <Text style={styles.CardHeadingText}>{month}</Text>
          <Text style={styles.timingText}>{timing}</Text>
        </View>
        <View style={styles.nameContainer}>
          <Text style={styles.CardHeadingText}>{name}</Text>
          <Text style={styles.subtitleText}>{subtitle}</Text>
        </View>
      </View>
      <Divider />
      <View style={styles.CustomerDetailsContainer}>
        <View style={styles.NameAddContainer}>
          <Text style={styles.NameAddHeading}>Customer:</Text>
          <Text style={{ width: "70%" }}>Text Appointment</Text>
        </View>
        <View style={styles.NameAddContainer}>
          <Text style={styles.NameAddHeading}>Address:</Text>
          <Text style={{ width: "70%" }}>
            81 Farrington Street, London EC4A 4BL
          </Text>
        </View>
      </View>
      <Divider />
      <View style={styles.HistoryHeadingContainer}>
        <Ionicons name="book-outline" size={30} color="black" />
        <Text style={styles.HistoryHeading}>CUSTOMER HISTORY</Text>
      </View>
      <View style={{ alignItems: "center" }}>
        <Text style={styles.coloredText}>
          Existing Customer: Repeated {repeated} times
        </Text>
      </View>
      <View style={FormStyles.bottom}>
        <TouchableOpacity>
          <View style={FormStyles.SignInButton}>
            <Text style={{ fontSize: 20 }}>CANCEL APPOINTMENT</Text>
          </View>
        </TouchableOpacity>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: "7%",
    backgroundColor: "white",
  },
  AppointmentDetailsContainer: {
    flexDirection: "row",
    marginHorizontal: 20,
    marginVertical: 30,
  },
  dayDateContainer: {
    width: "40%",
  },
  nameContainer: {
    width: "60%",
  },
  CardHeadingText: {
    fontSize: 16,
    fontWeight: "bold",
  },
  timingText: {
    fontSize: 14,
    color: "#e2c798",
    fontWeight: "bold",
  },
  coloredText: {
    fontSize: 14,
    fontWeight: "bold",
    color: "#e2c798",
  },
  subtitleText: {
    fontSize: 12,
    color: "rgba(189, 195, 199,1.0)",
  },
  CustomerDetailsContainer: {
    height: "30%",
    marginVertical: 20,
    marginHorizontal: 15,
  },
  NameAddContainer: {
    flexDirection: "row",
    marginBottom: 20,
  },
  NameAddHeading: { width: "25%", fontWeight: "bold" },
  HistoryHeadingContainer: {
    flexDirection: "row",
    marginVertical: 10,
    justifyContent: "center",
    alignItems: "center",
  },
  HistoryHeading: {
    marginLeft: 10,
    fontSize: 16,
    fontWeight: "bold",
  },
});

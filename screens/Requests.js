import React, { useState } from "react";
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  ScrollView,
} from "react-native";
import AppbarStyles from "../assets/styles/AppbarStyles";
import { Ionicons } from "@expo/vector-icons";
import Card from "../components/Card";

export default function Requests({ navigation }) {
  const [reqStatus, setReqStatus] = useState("empty");
  const ReqList = [
    {
      dayDate: "SUNDAY 3",
      month: "FEBRUARY",
      timing: "7:00 - 8:00 PM",
      name: "TEST APPT - Karastase Blow Dry and Day Mak-up",
      address: "81 Farrington Street, London EC4A 4BL",
    },
  ];
  return (
    <View style={styles.container}>
      <View style={AppbarStyles.TitleContainer}>
        <View style={AppbarStyles.TouchContainer}></View>
        <Text style={AppbarStyles.TitleText}>Requests</Text>
        <View style={AppbarStyles.rightContainer} />
      </View>
      {reqStatus == "empty" ? (
        <View>
          <View style={styles.message}>
            <Text
              style={{
                textAlign: "center",
                color: "#e2c798",
                fontWeight: "bold",
              }}
            >
              You have no job requests pending. Try again soon or tap the icon
              below to refresh
            </Text>
          </View>
          <TouchableOpacity
            style={{ alignItems: "center" }}
            onPress={() => setReqStatus("notEmpty")}
          >
            <Ionicons
              name="ios-refresh-circle-sharp"
              size={34}
              color="#e2c798"
            />
          </TouchableOpacity>
        </View>
      ) : (
        <View>
          <View style={styles.ReqHeading}>
            <Text style={styles.ReqHeadingText}>JOB REQUESTS</Text>
          </View>
          <ScrollView>
            {ReqList.map((item, index) => (
              <View key={index}>
                <Card>
                  <View style={styles.ListItemContainer}>
                    <View style={styles.dayDateContainer}>
                      <Text style={styles.CardHeadingText}>{item.dayDate}</Text>
                      <Text style={styles.CardHeadingText}>{item.month}</Text>
                      <Text style={styles.timingText}>{item.timing}</Text>
                    </View>
                    <View style={styles.nameContainer}>
                      <Text style={styles.CardHeadingText}>{item.name}</Text>
                      <Text style={styles.addressText}>{item.address}</Text>
                    </View>
                  </View>
                  <View style={styles.ButtonsContainer}>
                    <TouchableOpacity style={styles.DeclineButton}>
                      <View>
                        <Text style={styles.DeclineText}>DECLINE</Text>
                      </View>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.AcceptButton}>
                      <View>
                        <Text style={styles.AcceptText}>ACCEPT</Text>
                      </View>
                    </TouchableOpacity>
                  </View>
                </Card>
              </View>
            ))}
          </ScrollView>
        </View>
      )}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: "7%",
    backgroundColor: "white",
  },
  message: {
    marginBottom: 50,
    marginTop: "60%",
  },
  ReqHeading: {
    margin: 20,
  },
  ReqHeadingText: {
    fontSize: 18,
    fontWeight: "bold",
    color: "#e2c798",
  },
  ListItemContainer: {
    flexDirection: "row",
  },
  dayDateContainer: {
    width: "40%",
  },
  nameContainer: {
    width: "60%",
  },
  CardHeadingText: {
    fontSize: 16,
    fontWeight: "bold",
  },
  addressText: {
    fontSize: 12,
    color: "rgba(189, 195, 199,1.0)",
  },
  coloredText: {
    fontSize: 14,
    fontWeight: "bold",
    color: "#e2c798",
  },
  timingText: {
    fontSize: 14,
    color: "#e2c798",
    fontWeight: "bold",
  },
  ButtonsContainer: {
    flexDirection: "row",
    marginVertical: 15,
  },
  DeclineButton: {
    width: "40%",
    borderColor: "#e2c798",
    borderWidth: 1,
    margin: 10,
    padding: 10,
    justifyContent: "center",
    alignItems: "center",
  },
  AcceptButton: {
    width: "40%",
    backgroundColor: "#e2c798",
    margin: 10,
    padding: 10,
    justifyContent: "center",
    alignItems: "center",
  },
  DeclineText: {
    color: "#e2c798",
  },
  AcceptText: {
    color: "white",
  },
});

import React from "react";
import {
  View,
  Text,
  StyleSheet,
  Dimensions,
  TouchableOpacity,
} from "react-native";
import AppbarStyles from "../assets/styles/AppbarStyles";
import { CalendarList } from "react-native-calendars";
import { Entypo } from "@expo/vector-icons";

export default function Avalability() {
  return (
    <View style={AppbarStyles.SignContainer}>
      <View style={AppbarStyles.TitleContainer}>
        <View style={AppbarStyles.TouchContainer}></View>
        <Text style={AppbarStyles.TitleText}>Availability</Text>
        <View style={AppbarStyles.rightContainer} />
      </View>
      <CalendarList
        // Callback which gets executed when visible months change in scroll view. Default = undefined
        // Max amount of months allowed to scroll to the past. Default = 50
        pastScrollRange={50}
        // Max amount of months allowed to scroll to the future. Default = 50
        futureScrollRange={50}
        // Enable or disable scrolling of calendar list
        scrollEnabled={true}
        // Enable or disable vertical scroll indicator. Default = false
        showScrollIndicator={false}
        markingType={"multi-period"}
      />
      <TouchableOpacity>
        <View style={styles.FloatingButton}>
          <Entypo name="calendar" size={24} color="white" />
        </View>
      </TouchableOpacity>
    </View>
  );
}

const styles = StyleSheet.create({
  FloatingButton: {
    backgroundColor: "#e2c798",
    position: "absolute",
    width: 50,
    height: 50,
    alignItems: "center",
    justifyContent: "center",
    right: 30,
    bottom: 80,
    borderRadius: 50,
  },
});
